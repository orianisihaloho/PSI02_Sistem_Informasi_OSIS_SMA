/*
SQLyog Community v12.2.0 (64 bit)
MySQL - 5.6.24 : Database - osis2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`osis2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `osis2`;

/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `akun` */

/*Table structure for table `polling` */

DROP TABLE IF EXISTS `polling`;

CREATE TABLE `polling` (
  `id_polling` varchar(255) NOT NULL,
  PRIMARY KEY (`id_polling`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `polling` */

/*Table structure for table `register` */

DROP TABLE IF EXISTS `register`;

CREATE TABLE `register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alamat` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `register` */

insert  into `register`(`id`,`alamat`,`email`,`foto`,`nama`) values 
(1,'Jalan Pattimura Pematangsiantar','sman4siantar@sch.id','unduhan.png','SMA NEGERI 4 Pematangsiantar');

/*Table structure for table `t_berita` */

DROP TABLE IF EXISTS `t_berita`;

CREATE TABLE `t_berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(255) DEFAULT NULL,
  `isi_berita` text NOT NULL,
  `isi_singkat_berita` varchar(300) DEFAULT NULL,
  `judul_berita` varchar(255) NOT NULL,
  `last_update_berita` varchar(255) DEFAULT NULL,
  `status_berita` varchar(255) DEFAULT NULL,
  `opt_version` int(11) DEFAULT '0',
  `waktu_publish_berita` varchar(255) DEFAULT NULL,
  `t_divisi_id` int(11) DEFAULT NULL,
  `t_pengurus_id` int(11) DEFAULT NULL,
  `t_pengurus_approved_id` int(11) DEFAULT NULL,
  `tipe_pengumuman` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_berita`),
  KEY `FKq23267mau9pi7wivlo2hsrcyc` (`t_divisi_id`),
  KEY `FKcevv49i4o8ow08jb6xkomb8e6` (`t_pengurus_id`),
  KEY `FKk8bmhpu0hvj3yuvv87je2wuof` (`t_pengurus_approved_id`),
  CONSTRAINT `FKcevv49i4o8ow08jb6xkomb8e6` FOREIGN KEY (`t_pengurus_id`) REFERENCES `t_pengurus` (`id`),
  CONSTRAINT `FKk8bmhpu0hvj3yuvv87je2wuof` FOREIGN KEY (`t_pengurus_approved_id`) REFERENCES `t_pengurus` (`id`),
  CONSTRAINT `FKq23267mau9pi7wivlo2hsrcyc` FOREIGN KEY (`t_divisi_id`) REFERENCES `t_divisi` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `t_berita` */

insert  into `t_berita`(`id_berita`,`foto`,`isi_berita`,`isi_singkat_berita`,`judul_berita`,`last_update_berita`,`status_berita`,`opt_version`,`waktu_publish_berita`,`t_divisi_id`,`t_pengurus_id`,`t_pengurus_approved_id`,`tipe_pengumuman`) values 
(12,'tes.JPG','<p>TES</p>','<p>TES</p> ...','TES',NULL,'rejected',1,'2017-06-06',1,4,NULL,NULL),
(14,NULL,'<p>Sarah</p>\r\n<p>Oriani</p>\r\n<p>Christine</p>\r\n<p>Rahel</p>','<p>Sarah</p>\r\n<p>Oriani</p>\r\n<p>Christine</p>\r\n<p>Rahel</p> ...','List Mahasiswa Berpredikat Cumlaude',NULL,'accepted',0,'2018-05-03',NULL,2,NULL,NULL),
(17,'osis_sma.png','<p>osis sma</p>','<p>osis sma</p> ...','OSIS SMA',NULL,'accepted',0,'2018-05-03',NULL,2,NULL,NULL),
(21,'del.jpg','<p>Hari ini akan dilaksanakan kuliah umum bertempat di GSG del</p>',NULL,'KULIAH UMUM 13 JUNI 2018',NULL,NULL,0,'2018-06-12',NULL,NULL,NULL,'Pengumuman Pelaksanaan Kegiatan');

/*Table structure for table `t_divisi` */

DROP TABLE IF EXISTS `t_divisi`;

CREATE TABLE `t_divisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_divisi` varchar(255) DEFAULT NULL,
  `opt_version` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `t_divisi` */

insert  into `t_divisi`(`id`,`nama_divisi`,`opt_version`) values 
(1,'Pendidikan dan Penelitian',0),
(2,'Agama dan Kerohanian',0),
(3,'Seni dan Peralatan',0),
(4,'Pengabdian Masyarakat',0);

/*Table structure for table `t_forum` */

DROP TABLE IF EXISTS `t_forum`;

CREATE TABLE `t_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isi` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `opt_version` int(11) DEFAULT '0',
  `t_kategori_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkwnsgfnjyg2rov0qs3nryfhxn` (`t_kategori_id`),
  CONSTRAINT `FKkwnsgfnjyg2rov0qs3nryfhxn` FOREIGN KEY (`t_kategori_id`) REFERENCES `t_kategori_forum` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_forum` */

/*Table structure for table `t_kategori_forum` */

DROP TABLE IF EXISTS `t_kategori_forum`;

CREATE TABLE `t_kategori_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `opt_version` int(11) DEFAULT '0',
  `isi` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_kategori_forum` */

/*Table structure for table `t_pengumuman` */

DROP TABLE IF EXISTS `t_pengumuman`;

CREATE TABLE `t_pengumuman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) DEFAULT NULL,
  `isi_pengumuman` text NOT NULL,
  `judul_pengumuman` varchar(255) NOT NULL,
  `last_update_pengumuman` date DEFAULT NULL,
  `opt_version` int(11) DEFAULT '0',
  `waktu_publish_pengumuman` varchar(255) DEFAULT NULL,
  `t_pengurus_id` int(11) DEFAULT NULL,
  `status_pengumuman` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKf4mvob5jodpj76p72a6chdh47` (`t_pengurus_id`),
  CONSTRAINT `FKf4mvob5jodpj76p72a6chdh47` FOREIGN KEY (`t_pengurus_id`) REFERENCES `t_pengurus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `t_pengumuman` */

insert  into `t_pengumuman`(`id`,`file_path`,`isi_pengumuman`,`judul_pengumuman`,`last_update_pengumuman`,`opt_version`,`waktu_publish_pengumuman`,`t_pengurus_id`,`status_pengumuman`) values 
(12,'Data_Diri.docx','<p>TES</p>','TES',NULL,0,'2017-06-06',4,NULL);

/*Table structure for table `t_pengurus` */

DROP TABLE IF EXISTS `t_pengurus`;

CREATE TABLE `t_pengurus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(255) DEFAULT NULL,
  `opt_version` int(11) DEFAULT '0',
  `t_divisi_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa50v8jnpndmr3039y5wbre21m` (`t_divisi_id`),
  CONSTRAINT `FKa50v8jnpndmr3039y5wbre21m` FOREIGN KEY (`t_divisi_id`) REFERENCES `t_divisi` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `t_pengurus` */

insert  into `t_pengurus`(`id`,`jabatan`,`opt_version`,`t_divisi_id`) values 
(1,'Anggota OSIS SMA',0,NULL),
(2,'Sekretaris',0,NULL),
(3,'Pengurus Inti',0,NULL),
(4,'Penyelenggara Acara',0,NULL),
(5,'Unregistered user',0,NULL),
(6,'Ketua Divisi Seni & Peralatan',0,NULL),
(7,'Ketua Divisi Pengabdian Masyarakat',0,NULL),
(8,'Anggota Divisi Pendidikan & Penelitian',0,NULL),
(9,'Anggota Divisi Agama & Kerohanian',0,NULL),
(10,'Anggota Divisi Seni & Peralatan',0,NULL),
(11,'Anggota Divisi Pengabdian Masyarakat',0,NULL),
(12,'Anggota Himpunan',0,NULL);

/*Table structure for table `t_polling` */

DROP TABLE IF EXISTS `t_polling`;

CREATE TABLE `t_polling` (
  `id_polling` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(255) DEFAULT NULL,
  `isi_polling` varchar(255) NOT NULL,
  `isi_singkat_polling` varchar(255) DEFAULT NULL,
  `judul_polling` varchar(255) NOT NULL,
  `last_update_polling` varchar(255) DEFAULT NULL,
  `status_polling` varchar(255) DEFAULT NULL,
  `opt_version` int(11) DEFAULT '0',
  `waktu_publish_polling` varchar(255) DEFAULT NULL,
  `t_divisi_id` int(11) DEFAULT NULL,
  `t_pengurus_id` int(11) DEFAULT NULL,
  `t_pengurus_approved_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_polling`),
  KEY `FKofe6b7g9dd7umrq83n6t6jdue` (`t_divisi_id`),
  KEY `FKhm3dvs3kjwo6lh0mg808dkfpv` (`t_pengurus_id`),
  KEY `FK4w9bd5he12rxduah6is167kxf` (`t_pengurus_approved_id`),
  CONSTRAINT `FK4w9bd5he12rxduah6is167kxf` FOREIGN KEY (`t_pengurus_approved_id`) REFERENCES `t_pengurus` (`id`),
  CONSTRAINT `FKhm3dvs3kjwo6lh0mg808dkfpv` FOREIGN KEY (`t_pengurus_id`) REFERENCES `t_pengurus` (`id`),
  CONSTRAINT `FKofe6b7g9dd7umrq83n6t6jdue` FOREIGN KEY (`t_divisi_id`) REFERENCES `t_divisi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_polling` */

/*Table structure for table `t_role` */

DROP TABLE IF EXISTS `t_role`;

CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi_role` varchar(255) DEFAULT NULL,
  `opt_version` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `t_role` */

insert  into `t_role`(`id`,`deskripsi_role`,`opt_version`) values 
(1,'Anggota OSIS SMA',0),
(2,'Sekretaris',0),
(3,'Pengurus Inti',0),
(4,'Penyelenggara Acara',0),
(5,'Unregistered user',0);

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alamat` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_user` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_telepon` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `opt_version` int(11) DEFAULT '0',
  `t_pengurus_id` int(11) DEFAULT NULL,
  `t_role_id` int(11) DEFAULT NULL,
  `kelas_siswa` varchar(255) DEFAULT NULL,
  `email_siswa` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `nis` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_20e98noi9igwdgcqs6yy9fo2n` (`id_user`),
  KEY `FKopmfy5yjkrraxsxm2gxtsdai5` (`t_pengurus_id`),
  KEY `FK1v5ixstybo9em24d7y280tbmk` (`t_role_id`),
  CONSTRAINT `FK1v5ixstybo9em24d7y280tbmk` FOREIGN KEY (`t_role_id`) REFERENCES `t_role` (`id`),
  CONSTRAINT `FKopmfy5yjkrraxsxm2gxtsdai5` FOREIGN KEY (`t_pengurus_id`) REFERENCES `t_pengurus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`alamat`,`email`,`id_user`,`jenis_kelamin`,`nama`,`no_telepon`,`password`,`tanggal_lahir`,`opt_version`,`t_pengurus_id`,`t_role_id`,`kelas_siswa`,`email_siswa`,`foto`,`nis`) values 
(1,NULL,'iss14007@students.del.ac.id','001215007','XII-IA1','Zayn',NULL,'$2a$10$mvOQrLkk36NJr8qrQV2EnObeybn8aIRuS3QapCvKpOTWPRjKBjy6e','2017-02-15',6,1,1,'XII-IA1',NULL,NULL,NULL),
(2,'Jalan Bahagia Pardagangan','iss14021@students.del.ac.id','001215021','Perempuan','Paul','082235780977','$2a$10$rfMxBZA920VLaupyEIyrvO36Mc2S3omd.NgFEeyauOs.uzgUHBicC','1996-02-15',1,3,3,'XII-IA1',NULL,NULL,NULL),
(3,NULL,'iss14006@students.del.ac.id','001215006',NULL,'Zoella',NULL,'$2a$10$y2/9jJRgnVPCKjBWbPg9lev6dh1VbmtxPgv4VxH825OBTCT1T6oe2','1996-10-15',1,1,1,'XII-IA1',NULL,NULL,NULL),
(4,'Jalan Sudirman Medan ','iss14030@students.del.ac.id','001215030','Laki - laki','Alfie','085237489090','$2a$10$IyCi7Cs7kipxI04jjiXLN.T3Eu6uialCqzO9SGctuA.xGpdumdVrW','1996-01-15',1,4,4,'XII-IA1',NULL,NULL,NULL),
(5,'Sibolga','iss14036@students.del.ac.id','001215036','Laki - laki','Gigi','12345','$2a$10$2Cy7ezr04ApoQM9GaHkes.gO25Jvc833A0v6sfdYIS4Jnj1XMC0gK','2017-05-15',1,12,5,'XII-IA2',NULL,NULL,NULL),
(21,NULL,'iss14017@students.del.ac.id','001215017',NULL,'Doodie',NULL,'$2a$10$k6Sc02h7/XVAYw9Pua/wmu2VLDPVpigv4J.m7920/O2.3wnTqC5YS','1996-02-15',0,5,4,'XII-IA1',NULL,NULL,NULL),
(30,NULL,'iss14027@students.del.ac.id','001215027',NULL,'Anne',NULL,'$2a$10$WaoC.zY7/VJWPG9TOHiQPOnDeRXx30hTlq1Qlbqifttd8foxgeyTq','1996-02-15',0,6,4,'XII-IA2',NULL,NULL,NULL),
(44,NULL,'iss14048@students.del.ac.id','001215048',NULL,'Sam',NULL,'$2a$10$0uFySWHIZaIjQV8NJ2Jen.qNESRCkdc6gHTwWrd5bGsubcxxea6LC','1996-02-15',0,7,4,'XII-IA1',NULL,NULL,NULL),
(45,NULL,'iss14051@students.del.ac.id','001215051','XII-IA4','Dave',NULL,'$2a$10$qNNq279yn8A6nAsqAh6g6uXLehcmpTqNxPPYt2TxExWLoxhTI1IgO','1996-02-15',1,11,5,'XII-IA4',NULL,NULL,NULL),
(46,NULL,'iss14041@students.del.ac.id','001215041','XII-IS3','Ari',NULL,'$2a$10$/yJQ/0NegW.4O0AxooGTx.InzGF6A9Jl5l92HPAntV.p7eYNJwbui','1996-02-15',2,12,5,'XII-IS3',NULL,NULL,NULL),
(47,NULL,'TES','TES',NULL,'TES',NULL,'$2a$10$LbI2DMNrxh1RlQj2jrnhcOGCyZpNOwZEUT24upeiNx40Q3OIvKzlW','1996-02-15',0,2,2,'XII-IA1',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
